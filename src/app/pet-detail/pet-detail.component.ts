import { Component } from '@angular/core';
import { Pet } from '../pet';
import { PetService } from "../pet.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.css']
})
export class PetDetailComponent {
  pet: Pet | undefined;

  /**
   *
   */
  constructor(private route: ActivatedRoute, private petService: PetService) {  }

  ngOnInit(): void {
    this.getPet();
  }
  
  getPet(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.petService.getPet(id)
      .subscribe(pet => this.pet = pet);
  }
}
